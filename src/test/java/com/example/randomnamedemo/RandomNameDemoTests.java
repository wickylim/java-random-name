package com.example.randomnamedemo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class RandomNameDemoTests {

	@Test
	void contextLoads() {
		assertThat(RandomNameDemo.getName()).isNotNull();
	}

	@SpringBootApplication
	static class TestConfiguration {
	}
}
