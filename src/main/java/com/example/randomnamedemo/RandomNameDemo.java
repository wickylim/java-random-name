package com.example.randomnamedemo;

import com.github.javafaker.Faker;
import org.springframework.stereotype.Service;

@Service
public class RandomNameDemo {

	public static String getName() {
		Faker faker = new Faker();
		return faker.name().fullName();
	}

}
